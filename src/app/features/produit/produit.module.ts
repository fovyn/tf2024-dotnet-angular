import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProduitComponent } from './container/produit/produit.component';
import { ProduitListComponent } from './components/produit-list/produit-list.component';
import { ProduitDetailComponent } from './components/produit-detail/produit-detail.component';



@NgModule({
  declarations: [
    ProduitComponent,
    ProduitListComponent,
    ProduitDetailComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [ProduitComponent]
})
export class ProduitModule { }
