import { Component, Input } from '@angular/core';

@Component({
  selector: 'produit-detail',
  templateUrl: './produit-detail.component.html',
  styleUrl: './produit-detail.component.scss'
})
export class ProduitDetailComponent {
  @Input('product') product!: any
}
