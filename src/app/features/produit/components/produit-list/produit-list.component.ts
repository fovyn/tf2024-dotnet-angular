import { Component, EventEmitter, OnInit, Output, signal } from '@angular/core';

@Component({
  selector: 'produit-list',
  templateUrl: './produit-list.component.html',
  styleUrl: './produit-list.component.scss'
})
export class ProduitListComponent implements OnInit {
  @Output('onSelect') onSelectEvent = new EventEmitter<any>()

  items = signal<any[]>([])

  ngOnInit(): void {
      this.items.set([
        { id: 1, label: "Produit 1", price: 42.42 },
        { id: 2, label: "Produit 2", price: 42.42 },
        { id: 3, label: "Produit 3", price: 42.42 },
        { id: 4, label: "Produit 4", price: 42.42 },
      ])
  }

  onSelectHandler(item: any) {
    console.log("SELECTED: ", item)
    this.onSelectEvent.emit(item)
  }

  onRemoveHandler(item: any) {
    const items = this.items()

    const index = items.findIndex(it => it.id === item.id);
    
    items.splice(index, 1)
    this.items.update(() => [...items])
  }
}
