import { HttpClient } from '@angular/common/http';
import { Component, OnInit, inject } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit {
  title = 'tfdotnet2024_angular';
  private $http = inject(HttpClient)

  ngOnInit(): void {
    this.$http.get<any[]>('http://localhost:3000/books').subscribe(data => console.log(data))
  }
}
